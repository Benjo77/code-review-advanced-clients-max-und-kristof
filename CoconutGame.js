// Code Review von Team "Neue Karte"
// Startpunkt siehe "script.js"

class CoconutGame {
  static allInstances = [];

  // Vorschlag: roomId
  constructor(roomid) {
    // Größtenteils gutes Naming
    this.users = [];
    this.roomid = roomid;
    // Variable wird nicht benutzt
    this.ongoingRound = false;
    this.usersInRound = [];
    //Vorschlag: playerArray ausschreiben
    this.playerArr = [];
  }

  // Sehr gut: Alle nachfolgenden Funktionen besitzen genau eine Aufgabe, keine "side effects"
  static get(uuid) {
    // Vorschlag: const verwenden, wo möglich
    for(var i=0; i<CoconutGame.allInstances.length; i++) {
      let game = CoconutGame.allInstances[i];
      // Vorschlag: statt "==" an dieser Stelle besser "===" verwenden
      if(game.roomid == uuid) {
        return game;
      }
    }
    let newGame = new CoconutGame(uuid);
    CoconutGame.allInstances.push(newGame);
    return newGame;
  }

  addUser(user) {
    this.users.push(user);
  }

  removeUser(usr) {
    this.users = this.users.filter((user) => user !== usr);
  }

  start(userId) {
    // Critical: Es wird nicht überprüft, ob bereits eine Runde läuft
    this.playerArr[0] = userId;
    this.ongoingRound = true;
    this.usersInRound = [...this.users];

    // Vorschlag: const verwenden wo möglich
    var index = this.usersInRound.indexOf(userId);
    if (index > -1) {
      this.usersInRound.splice(index, 1);
    }

    if(this.usersInRound.length > 0) {
      this.playerArr[1] = this.usersInRound.pop();
    } else {
      return false;
    }

    return this.playerArr;
  }

  // Vorschlag: Ggf. umbenennen -> "nextUser" oder "nextPlayer"
  next() {
    this.playerArr[0] = this.playerArr[1];
    this.ongoingRound = true;

    if(this.usersInRound.length > 0) {
      this.playerArr[1] = this.usersInRound.pop();
    } else {
      return false;
    }

    return this.playerArr;
  }
}

module.exports = CoconutGame;
