/* Code Review von Team "Neue Karte"

####### Spezifische Anmerkungen als Kommentare im jeweiligen Abschnitt. #######

Folgende Anmerkungen gelten für Script.js, Server.js und CoconutGame.js

####### Allgemeine Anmerkungen: #######
    Readability:
        - Vorschlag: Für die Übersichtlichkeit/Erweiterbarkeit/Single Resposibility wäre es ggf. hilfreich die "script.js"
          ihrer Logik entsprechend in mehrere Dateien/Klassen aufzuteilen.
          Denkbar wären bspw. folgende Aufteilung:
            - Event Logik für Event-/Click-Listener
            - Socket Logik
            - Generelle Game-Logik (enableVideo, ...)

        - Sehr gut: Funktionen sind relativ kurz, sinnvoll benannt und selbsterklärend

        - Alle Funktionen sind sauber in "queries" und "commands" (modifiers und mutators) aufgeteilt

        - Single Responsibility bei nachfolgenden Funktionen stets erfüllt: Alle Funktionen haben eine Aufgabe und keine "side effects"

    Naming Consistencies:
        - Sehr gutes Naming. Für die meisten Variablen und Funktionen ist anhand des Namens deren Aufgabe direkt ersichtlich

        - Alles ist "pronouncable". Namen sind zudem gut "searchable".

    Simplicity:
        - Für die Komplexität des "CoconutGames" ist die Codebase simpel und kompakt.

*/

// Sehr gut: Nach Möglichkeit "Const" verwendet, nicht "var"/"let"
// Sehr gut: Naming sehr intuitiv
// Vorschlag: Socket ggf. in eine eigene Datei auslagern, zwecks Übersichtlichkeit
const socket = io("/");
const videoGrid = document.getElementById("video-grid");
const myVideo = document.createElement("video");
const showChat = document.querySelector("#showChat");
//Vorschlag: Button könnte ausgeschrieben werden
const backBtn = document.querySelector(".header__back");
myVideo.muted = true;

// Vorschlag: Gesamte Event Logik ggf. in eine eigene Datei auslagern, zwecks Übersichtlichkeit
backBtn.addEventListener("click", () => {
    document.querySelector(".main__left").style.display = "flex";
    document.querySelector(".main__left").style.flex = "1";
    document.querySelector(".main__right").style.display = "none";
    document.querySelector(".header__back").style.display = "none";
});

showChat.addEventListener("click", () => {
    document.querySelector(".main__right").style.display = "flex";
    document.querySelector(".main__right").style.flex = "1";
    document.querySelector(".main__left").style.display = "none";
    document.querySelector(".header__back").style.display = "block";
});

// Vorschlag: Evtl. eine Init-Funktion oder ähnliches ergänzen, die die "OnInit" Aufgaben abarbeitet. Wirkt an dieser Stelle etwas verloren.
const user = prompt("Enter your name");

const peer = new Peer(undefined, {
    path: "/peerjs",
    host: "/",
    port: PORT,
    debug: true,
});

// Vorschlag: Debugging Code entfernen, sobald nicht mehr benötigt
console.log(PORT)

let myVideoStream;
// Unklar: Werden hier die mediaDevices der User initialisiert?
// Vorschlag: Ggf. ebenfalls in die Init-Funktion packen, sofern möglich
navigator.mediaDevices.getUserMedia({
    audio: true,
    video: true,
}).then((stream) => {
    myVideoStream = stream;
    addVideoStream(myVideo, stream, userIdLocal);

    peer.on("call", (call) => {
        call.answer(stream);
        const video = document.createElement("video");
        call.on("stream", (userVideoStream) => {
            addVideoStream(video, userVideoStream);
        });
    });

    socket.on("user-connected", async (userId) => {
        // Unklar: Worauf wird hier genau gewartet? Hier wäre ggf. auch ein Kommentar hilfreich für schnelleres Verständnis
        // Vorschlag:   Timeout Funktion ist durchaus valide, kann aber unter Umständen zu Problemen führen.
        //              Wir gehen davon aus, dass durch den Timeout auf den Video-Stream gewartet wird.
        //              Sollte dieser nach 1000ms nicht bereit sein, entstehen Probleme.
        //              Schöner wäre eine dynamische Lösung für den Timer.
        // Unklar: Ist "r" ein Standard-Variablen Name? Falls ja, gut so, ansonnsten wäre ein besseres Naming möglich.
        await new Promise(r => setTimeout(r, 1000));
        connectToNewUser(userId, stream);
    });
});

// Sehr gut: function-naming ist klar
const connectToNewUser = (userId, stream) => {
    const call = peer.call(userId, stream);
    const video = document.createElement("video");
    // Vorschlag: Debugging Code entfernen, sobald nicht mehr benötigt
    console.log('connected to new user', userId)
    call.on("stream", (userVideoStream) => {
        // Unklar: Formulierung des Logs ähnlich zu vorigem, ggf. genauer beschreiben, wie "connect stream/video" o.Ä.
        console.log('connect to new user', userId)
        addVideoStream(video, userVideoStream, userId);
    });
};

let userIdLocal;
// Vorschlag: Callback Funktionen (peer / socket) der Übersichtlichkeit halber in eigene Datei auslagern
peer.on("open", (id) => {
    userIdLocal = id;
    socket.emit("join-room", ROOM_ID, id, user);
});

// Vorschlag: Event Logik ggf. in eine eigene Datei auslagern (siehe oben)
window.addEventListener("beforeunload", function() {
    socket.emit("leave-room", ROOM_ID, userIdLocal);
});

// Vorschlag: Code ggf. auslagern (siehe oben)
peer.on('close', () => {
    socket.emit("leave-room", ROOM_ID, userIdLocal);
});

// Sehr gut: function-naming ist klar; Ggf. sogar "addVideoStreamToView"
const addVideoStream = (video, stream, id) => {
    video.srcObject = stream;
    // Vorschlag: Debugging Code entfernen, sobald nicht mehr benötigt
    console.log(id)
    if (id) video.id = id;
    video.addEventListener("loadedmetadata", () => {
        video.play();
        videoGrid.append(video);
    });
};
// Naming hier etwas unklar. "text" und "messages" nicht klar unterscheidbar. Was kommt von anderen, was von mir?
//      Vorschlag: Naming deutlicher gestalten
let text = document.querySelector("#chat_message");
//      Vorschlag: Naming deutlicher gestalten, beispielsweise "sendButton"
let send = document.getElementById("send");
//      Vorschlag: Naming deutlicher gestalten
let messages = document.querySelector(".messages");

// Vorschlag: Code ggf. auslagern (siehe oben)
send.addEventListener("click", (e) => {
    if (text.value.length !== 0) {
        socket.emit("message", text.value);
        text.value = "";
    }
});

// Vorschlag: Code ggf. auslagern (siehe oben)
// Geringfügige Code Doppelung.
text.addEventListener("keydown", (e) => {
    if (e.key === "Enter" && text.value.length !== 0) {
        socket.emit("message", text.value);
        text.value = "";
    }
});

// Beschriebene Variablen werden in folgenden Funktionen in anderer Reihenfolge aufgerufen.
// Vorschlag: An alle vier Variablen "Button" anhängen
// Inkonsistentes HTML-Naming (teils mit Button, teils ohne)
const startWhisper = document.querySelector("#startWhisper");
const inviteButton = document.querySelector("#inviteButton");
const muteButton = document.querySelector("#muteButton");
const stopVideo = document.querySelector("#stopVideo");

startWhisper.addEventListener("click", (e) => {
    // Vorschlag: Debugging Code entfernen
    console.log("button gedrück alter"); //typo
    socket.emit("startWhisper");
});

// Vorschlag: Code ggf. auslagern (siehe oben)
muteButton.addEventListener("click", () => {
    const enabled = myVideoStream.getAudioTracks()[0].enabled;
    // Vorschlag: Ggf. "mute-symbol" togglen, anstelle des if/else
    if (enabled) {
        myVideoStream.getAudioTracks()[0].enabled = false;
        html = `<i class="fas fa-microphone-slash"></i>`;
        muteButton.classList.toggle("background__red");
        muteButton.innerHTML = html;
    } else {
        myVideoStream.getAudioTracks()[0].enabled = true;
        html = `<i class="fas fa-microphone"></i>`;
        muteButton.classList.toggle("background__red");
        muteButton.innerHTML = html;
    }
});

// Vorschlag: Code ggf. auslagern (siehe oben)
stopVideo.addEventListener("click", () => {
    const enabled = myVideoStream.getVideoTracks()[0].enabled;
    if (enabled) {
        myVideoStream.getVideoTracks()[0].enabled = false;
        html = `<i class="fas fa-video-slash"></i>`;
        stopVideo.classList.toggle("background__red");
        stopVideo.innerHTML = html;
    } else {
        myVideoStream.getVideoTracks()[0].enabled = true;
        html = `<i class="fas fa-video"></i>`;
        stopVideo.classList.toggle("background__red");
        stopVideo.innerHTML = html;
    }
});

// Vorschlag: Code ggf. auslagern (siehe oben)
inviteButton.addEventListener("click", (e) => {
    prompt(
        "Copy this link and send it to people you want to meet with",
        window.location.href
    );
});

// Vorschlag: Code ggf. auslagern (siehe oben)
socket.on("createMessage", (message, userName) => {
    messages.innerHTML =
        messages.innerHTML +
        `<div class="message">
        <b><i class="far fa-user-circle"></i> <span> ${
            userName === user ? "me" : userName
        }</span> </b>
        <span>${message}</span>
    </div>`;
});

// Vorschlag: Code ggf. auslagern (siehe oben)
// Vorschlag: socket message "destroyUser" in "removeUser" oder sogar "removeUserVideo" umbenennen
socket.on("destroyUser", (id) => {
        //Vorschlag: Debugging Code ggf. entfernen
    console.log(userIdLocal, id)
    const video = document.getElementById(id)
    console.log('remove', video)
    if (video) video.remove()
});

// Für die vier Folgenden Funktionen:
//      Sehr gutes function-naming (außer typo)
//      Vorschlag: Funktionen ggf. auslagern (siehe oben)
//      Einige Code-Dopplungen in nachfolgenden Funktionen
function enableVideo() {
    myVideoStream.getVideoTracks()[0].enabled = true;
    html = `<i class="fas fa-video"></i>`;
    stopVideo.classList.toggle("background__red");
    stopVideo.innerHTML = html;
}

function disableVideo() {
    myVideoStream.getVideoTracks()[0].enabled = false;
    html = `<i class="fas fa-video-slash"></i>`;
    stopVideo.classList.toggle("background__red" )
    stopVideo.innerHTML = html;
}

function enableAudio() {
    myVideoStream.getAudioTracks()[0].enabled = true;
    html = `<i class="fas fa-microphone"></i>`;
    muteButton.classList.toggle("background__red");
    muteButton.innerHTML = html;
}

function diasbleAudio() {
    myVideoStream.getAudioTracks()[0].enabled = false;
    html = `<i class="fas fa-microphone-slash"></i>`;
    muteButton.classList.toggle("background__red");
    muteButton.innerHTML = html;
}

socket.on("startGame", (currentPlayers) => {
    // Vorschlag: statt "==" an dieser Stelle besser "===" verwenden
    if(currentPlayers.indexOf(userIdLocal) == 0) {
        enableVideo();
        enableAudio();
    }
    // Vorschlag: statt "==" an dieser Stelle besser "===" verwenden
    else if(currentPlayers.indexOf(userIdLocal) == 1) {
        diasbleAudio(); //typo
        enableVideo();
    }
    else {
        disableVideo();
        diasbleAudio();
    }
});

socket.on("nextRound", (currentPlayers) => {
    // Vorschlag: statt "==" an dieser Stelle besser "===" verwenden
    if(currentPlayers.indexOf(userIdLocal) == 0) {
        enableVideo();
        enableAudio();
    }
    // Vorschlag: statt "==" an dieser Stelle besser "===" verwenden
    else if(currentPlayers.indexOf(userIdLocal) == 1) {
        diasbleAudio();
        enableVideo();
    }
    else {
        disableVideo();
        diasbleAudio();
    }
});

socket.on("endGame", () => {
    enableVideo();
    enableAudio();
});
